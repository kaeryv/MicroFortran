package io.quadrasoft.mufortran.app;

import io.quadrasoft.mufortran.app.forms.LoadManager;
import io.quadrasoft.mufortran.app.forms.WelcomePanel;
import io.quadrasoft.mufortran.fortran.FileTypesManager;
import io.quadrasoft.mufortran.general.Log;
import io.quadrasoft.mufortran.general.Session;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

public class Main
{
    private static LoadManager loading = null;
    private static WelcomePanel splashScreen = null;
    private static MainFrame app = null;

    public static void main(String[] args)
    {
        System.setProperty("awt.useSystemAAFontSettings", "on");
        System.setProperty("swing.aatext", "true");


        SwingUtilities.invokeLater(() ->
        {
            iniFrames();
            loading.setLocationRelativeTo(null);
            loading.loadConfig();
            LoadManager.loadTheme();

            app = new MainFrame();

            app.toFront();
            app.updateTrees();
            app.setVisible(true);

            if (args.length > 0)
            {
                for (String argument : args)
                {
                    if (FileTypesManager.isFortranSource(argument) || FileTypesManager.isProjectFile(argument))
                    {
                        app.open(argument);
                        splashScreen.close();
                        splashScreen = null;
                    }
                    else
                    {
                        Log.send("Error: Unsupported file specified : \"" + argument + "\"");
                    }
                }

            }
            else
            {
                app.toFront();
                splashScreen.loadRecents();
                splashScreen.setVisible(true);
                splashScreen.setLocationRelativeTo(null);

            }
            loading.close();
            loading = null;
        });
    }

    private static void iniFrames()
    {
        loading = new LoadManager();
        splashScreen = new WelcomePanel();
        splashScreen.addWindowListener(new WindowAdapter()
        {

            @Override
            public void windowClosing(WindowEvent e)
            {
                app.updateTrees();
                splashScreen.close();
                splashScreen = null;
                if (Session.autoCheck())
                {
                    try
                    {
                        LoadManager.checkVersion();
                    } catch (IOException exception)
                    {
                        Log.send("Error: Could not get version from github, check your internet connection.");
                        exception.printStackTrace();
                    }
                }
            }
        });
    }
}
