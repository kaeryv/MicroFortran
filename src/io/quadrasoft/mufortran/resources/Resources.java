package io.quadrasoft.mufortran.resources;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.print.Doc;
import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Resources
{
    private static List<Resource> resList = new ArrayList<Resource>();
    public static boolean ResourcesLoaded = false;

    public static final ImageIcon getImageResource(String name)
    {
        for (Resource res : resList)
        {
            if (res.getName().equalsIgnoreCase(name))
                return res.getImage();
        }
        return null;
    }

    public static final void load()
    {
        InputStream xmlstream = Resources.class.getResourceAsStream("db/reslist.xml");

        if (true)
        {
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

            try
            {

                final DocumentBuilder builder = factory.newDocumentBuilder();
                final Document document = builder.parse(xmlstream);
                final Element root = document.getDocumentElement();

                if (root.getTagName() != "reslist")
                {
                    throw new Exception("Bad resources file.");
                }

                final NodeList nodes = root.getChildNodes();
                final int length = nodes.getLength();

                for (int i = 0; i < length; i++)
                {
                    if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE)
                    {
                        final Element node = (Element) nodes.item(i);

                        if (node.getTagName() == "icon")
                        {
                            resList.add(new Resource(node.getTextContent()));
                        }
                    }
                }
            } catch (ParserConfigurationException e)
            {
                e.printStackTrace();
            } catch (IOException e)
            {
                e.printStackTrace();
            } catch (SAXException e)
            {
                e.printStackTrace();
            } catch (Exception e)
            {
                e.printStackTrace();
            }

            ResourcesLoaded = true;
        }
        else
        {
            System.out.println("Failed to load .xml");
        }
    }

    public static class Resource
    {
        private String name = new String("");
        private ImageIcon image;

        public Resource(String path)
        {
            this.setImage(new ImageIcon(this.getClass().getResource("icons/" + path)));
            this.setName(path.substring(0, path.lastIndexOf(".")));
        }

        public ImageIcon getImage()
        {
            return image;
        }

        public void setImage(ImageIcon image)
        {
            this.image = image;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }
    }
}
