package io.quadrasoft.mufortran.app.editor;

import io.quadrasoft.mufortran.app.forms.QSearcher;
import io.quadrasoft.mufortran.app.forms.QSearcherReplacer;
import io.quadrasoft.mufortran.fortran.BinaryManager;
import io.quadrasoft.mufortran.general.Log;
import io.quadrasoft.mufortran.general.Session;
import io.quadrasoft.mufortran.resources.Resources;
import io.quadrasoft.mufortran.resources.Strings;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.*;

@SuppressWarnings("serial")
public class EditorTab extends JPanel implements ActionListener, KeyListener, MouseListener
{

    private final JToggleButton debugButton = new JToggleButton();
    private final JLabel infoSelection = new JLabel();
    private final JTabbedPane utilsPane = new JTabbedPane();
    private final JPanel consoleStuff = new JPanel();
    public JTabbedPane editorPane = new JTabbedPane();
    Map<ButtonAction, JButton> toolBarButtons = new LinkedHashMap<>();
    private JToolBar toolBar;

    public EditorTab()
    {
        super();
        this.setLayout(new BorderLayout());
        this.initComponents();
        this.updateButtons();

        toolBarButtons.put(ButtonAction.NEW, new JButton());
        toolBarButtons.put(ButtonAction.OPEN, new JButton());
        toolBarButtons.put(ButtonAction.SAVE, new JButton());
        toolBarButtons.put(ButtonAction.SAVEAS, new JButton());
        toolBarButtons.put(ButtonAction.CLOSE, new JButton());
        toolBarButtons.put(ButtonAction.UNDO, new JButton());
        toolBarButtons.put(ButtonAction.REDO, new JButton());
        toolBarButtons.put(ButtonAction.CUT, new JButton());
        toolBarButtons.put(ButtonAction.COPY, new JButton());
        toolBarButtons.put(ButtonAction.PASTE, new JButton());
        toolBarButtons.put(ButtonAction.SEARCH, new JButton());
        toolBarButtons.put(ButtonAction.REPLACE, new JButton());
        toolBarButtons.put(ButtonAction.BUILD, new JButton());
        toolBarButtons.put(ButtonAction.COMPILEF, new JButton());
        toolBarButtons.put(ButtonAction.LINK, new JButton());
        toolBarButtons.put(ButtonAction.RUN, new JButton());
        toolBarButtons.put(ButtonAction.CLEAR, new JButton());

        Map<ButtonAction, String> buttonIcons = new HashMap<ButtonAction, String>();

        buttonIcons.put(ButtonAction.NEW, "icon.newfile");
        buttonIcons.put(ButtonAction.OPEN, "icon.folder");
        buttonIcons.put(ButtonAction.SAVE, "icon.save");
        buttonIcons.put(ButtonAction.SAVEAS, "icon.saveas");
        buttonIcons.put(ButtonAction.UNDO, "icon.undo");
        buttonIcons.put(ButtonAction.REDO, "icon.redo");
        buttonIcons.put(ButtonAction.COPY, "icon.copy");
        buttonIcons.put(ButtonAction.CUT, "icon.cut");
        buttonIcons.put(ButtonAction.PASTE, "icon.paste");
        buttonIcons.put(ButtonAction.SEARCH, "icon.magnifier");
        buttonIcons.put(ButtonAction.REPLACE, "icon.magnifier");
        buttonIcons.put(ButtonAction.BUILD, "icon.build");
        buttonIcons.put(ButtonAction.LINK, "icon.link");
        buttonIcons.put(ButtonAction.COMPILEF, "icon.build_file");
        buttonIcons.put(ButtonAction.RUN, "icon.play");
        buttonIcons.put(ButtonAction.CLEAR, "icon.eraser");
        buttonIcons.put(ButtonAction.CLOSE, "icon.cancel");


        Set set = toolBarButtons.entrySet();
        Iterator iterator = set.iterator();

        while (iterator.hasNext())
        {
            Map.Entry entry = (Map.Entry) iterator.next();
            JButton button = (JButton) entry.getValue();
            button.setOpaque(false);
            button.setIcon(Resources.getImageResource(buttonIcons.get(entry.getKey())));
            button.addActionListener(this);
            button.setBorder(BorderFactory.createEmptyBorder());
            button.setPreferredSize(new Dimension(30, 40));
            button.setEnabled(false);
            toolBar.add(button);
            toolBar.addSeparator();
        }
        toolBar.add(debugButton);

        updateButtons();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (editorPane.getSelectedIndex() != -1)
            ((EditorFile) editorPane.getSelectedComponent()).getEditor().requestFocus();

        final Object source = e.getSource();

        if (source.equals(toolBarButtons.get(ButtonAction.UNDO)))
        {
            undo();
        }
        else if (source.equals(toolBarButtons.get(ButtonAction.REDO)))
        {
            redo();
        }
        else if (source.equals(toolBarButtons.get(ButtonAction.NEW)))
        {
            newFile();
        }
        else if (source.equals(toolBarButtons.get(ButtonAction.OPEN)))
        {
            String filename;
            String directory = Session.getWorkDir();
            JFileChooser chooser = new JFileChooser(directory);
            FileFilter filter = new FileNameExtensionFilter("Fortran source", "f", "f90", "f95", "F", "F90");
            chooser.removeChoosableFileFilter(chooser.getFileFilter());
            chooser.addChoosableFileFilter(filter);
            filter = new FileNameExtensionFilter("Other text files", "txt", "mod", "log", "info", "dat");
            chooser.addChoosableFileFilter(filter);
            if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
            {
                filename = chooser.getSelectedFile().getAbsolutePath();
                open(filename.replaceAll("\\\\", "/"));
                // ((EditorFile)
                // editorPane.getSelectedComponent()).undoManager.discardAllEdits();
                ((EditorFile) editorPane.getSelectedComponent()).getEditor().addMouseListener(this);
                updateButtons();
            }
        }
        else if (source.equals(toolBarButtons.get(ButtonAction.SAVE)))
        {
            if (editorPane.getSelectedIndex() != -1)
            {
                ((EditorFile) editorPane.getSelectedComponent()).save();
                toolBarButtons.get(ButtonAction.SAVE).setEnabled(false);
                toolBarButtons.get(ButtonAction.SAVEAS).setEnabled(false);
                if (((EditorFile) editorPane.getSelectedComponent()).isNeedReopen())
                {
                    String temp1 = ((EditorFile) editorPane.getSelectedComponent()).getPath();
                    editorPane.remove((editorPane.getSelectedComponent()));
                    open(temp1);
                }
            }

        }
        else if (source.equals(toolBarButtons.get(ButtonAction.SAVEAS)))
        {
            if (editorPane.getSelectedIndex() != -1)
                ((EditorFile) editorPane.getSelectedComponent()).saveAs();
            toolBarButtons.get(ButtonAction.SAVE).setEnabled(false);
            toolBarButtons.get(ButtonAction.SAVEAS).setEnabled(false);
        }
        else if (source.equals(toolBarButtons.get(ButtonAction.COPY)))
        {
            if (editorPane.getSelectedIndex() != -1)
                ((EditorFile) editorPane.getSelectedComponent()).getEditor().copy();
        }
        else if (source.equals(toolBarButtons.get(ButtonAction.CUT)))
        {
            if (editorPane.getSelectedIndex() != -1)
            {
                ((EditorFile) editorPane.getSelectedComponent()).getEditor().cut();
                updateButtons();
                ((EditorFile) editorPane.getSelectedComponent()).getEditor().colorise(false);
            }
        }
        else if (source.equals(toolBarButtons.get(ButtonAction.CLOSE)))
        {
            close();
        }
        else if (source.equals(toolBarButtons.get(ButtonAction.PASTE)))
        {
            if (editorPane.getSelectedIndex() != -1)
            {
                ((EditorFile) editorPane.getSelectedComponent()).getEditor().paste();
                ((EditorFile) editorPane.getSelectedComponent()).getEditor().colorise(false);
                updateButtons();
            }
        }
        else if (source.equals(toolBarButtons.get(ButtonAction.SEARCH)))
        {
            if (editorPane.getSelectedIndex() != -1)
            {
                new QSearcher(((EditorFile) editorPane.getSelectedComponent()).getEditor());
                ((EditorFile) editorPane.getSelectedComponent()).getEditor().requestFocus();
            }
        }
        else if (source.equals(toolBarButtons.get(ButtonAction.REPLACE)))
        {
            if (editorPane.getSelectedIndex() != -1)
            {
                new QSearcherReplacer(((EditorFile) editorPane.getSelectedComponent()).getEditor());
                ((EditorFile) editorPane.getSelectedComponent()).getEditor().requestFocus();
            }
        }
        else if (source.equals(toolBarButtons.get(ButtonAction.LINK)))
        {
            if (Session.getActiveProject() != null)
            {
                if (iHaveEditedFilesHere())
                {
                    if (JOptionPane.showConfirmDialog(new JFrame(),
                            "Better save changes before link.") == JOptionPane.OK_OPTION)
                    {
                        saveAll();
                    }
                }
                BinaryManager.bindProject(Session.getActiveProject());
                BinaryManager.linkProject();
            }
            else
            {
                Log.send("Open a project first.");
            }
        }
        else if (source.equals(toolBarButtons.get(ButtonAction.CLEAR)))
        {
            if (Session.getActiveProject() != null)
            {
                String project_root = Session.getActiveProject().getPath();
                File folder = new File(project_root + Session.getActiveProject().getObjectPath());
                File allBuildFiles[] = folder.listFiles();

                for (File build_file : allBuildFiles)
                {
                    String build_filename = build_file.getName();
                    if (build_filename.endsWith(".mod"))
                    {
                        build_file.delete();
                    }
                }
            }
        }
        else if (source.equals(toolBarButtons.get(ButtonAction.COMPILEF)))
        {
            if (Session.getActiveProject() != null)
            {
                if (editorPane.getSelectedComponent() != null)
                {
                    if (((EditorFile) editorPane.getSelectedComponent()).isEdited())
                    {
                        if (JOptionPane.showConfirmDialog(new JFrame(),
                                "Better save this file before compilation.") == JOptionPane.OK_OPTION)
                        {
                            ((EditorFile) editorPane.getSelectedComponent()).save();
                        }
                    }
                    BinaryManager.bindProject(Session.getActiveProject());
                    BinaryManager.debug(debugButton.isSelected());
                    BinaryManager.compileFile(((EditorFile) editorPane.getSelectedComponent()).getPath());
                }

            }
            else
            {
                Log.send("Open a project first.");
            }
        }
        if (source.equals(toolBarButtons.get(ButtonAction.BUILD)))
        {
            if (Session.getActiveProject() != null)
            {
                if (iHaveEditedFilesHere())
                {
                    if (JOptionPane.showConfirmDialog(new JFrame(),
                            "Better save changes before compilation.") == JOptionPane.OK_OPTION)
                    {
                        saveAll();
                    }
                }
                BinaryManager.bindProject(Session.getActiveProject());
                BinaryManager.debug(debugButton.isSelected());
                BinaryManager.compileProject();
            }
            else
            {
                Log.send("Open a project first.");
            }
        }
        else if (source.equals(toolBarButtons.get(ButtonAction.RUN)))
        {
            if (Session.getActiveProject() != null)
            {
                BinaryManager.bindProject(Session.getActiveProject());
                BinaryManager.launchProject();
                ;
            }
            else
            {
                Log.send("Open a project first.");
            }
        }
        else if (e.getSource().equals(debugButton))
        {
            if (debugButton.isSelected())
                debugButton.setIcon(Resources.getImageResource("icon.redbug"));
            else
                debugButton.setIcon(Resources.getImageResource("icon.bug"));
        }
    }

    public void close()
    {

        if (editorPane.getSelectedIndex() != -1)
        {
            Log.send("Closing active file");
            if (((EditorFile) editorPane.getSelectedComponent()).callSave())
                editorPane.remove(editorPane.getSelectedIndex());
        }
        else
        {
            Log.send("Nothing to close.");
        }

    }

    private boolean iHaveEditedFilesHere()
    {
        for (Component file : editorPane.getComponents())
        {
            if (((EditorFile) file).isEdited())
                return true;
        }
        return false;
    }

    private void initComponents()
    {
        Border emptyBorder = BorderFactory.createEmptyBorder();

        infoSelection.setText(Strings.s("Forms:NoSelection"));
        debugButton.addActionListener(this);
        debugButton.setIcon(Resources.getImageResource("icon.bug"));
        debugButton.setOpaque(false);
        debugButton.setBorder(emptyBorder);
        debugButton.setToolTipText("Enable debug configuration");

        toolBar = new JToolBar();

        toolBar.setFloatable(true);
        toolBar.setRollover(true);

        utilsPane.setPreferredSize(new Dimension(800, 200));

        consoleStuff.setLayout(new BorderLayout());
        consoleStuff.add(infoSelection, BorderLayout.CENTER);
        consoleStuff.setBorder(BorderFactory.createEtchedBorder());
        editorPane.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent arg0)
            {
                updateButtons();
            }
        });

        this.add(editorPane);
        this.add(consoleStuff, BorderLayout.SOUTH);
        this.add(toolBar, BorderLayout.NORTH);
    }

    public boolean isOpenend(String path)
    {
        for (Component temp : editorPane.getComponents())
        {
            if (((EditorFile) temp).getPath().equalsIgnoreCase(path))
            {
                editorPane.setSelectedComponent(temp);
                return true;
            }
        }
        return false;
    }

    @Override
    public void keyPressed(KeyEvent e)
    {

    }

    @Override
    public void keyReleased(KeyEvent e)
    {

        updateButtons();
    }

    @Override
    public void keyTyped(KeyEvent e)
    {
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
    }

    @Override
    public void mouseEntered(MouseEvent e)
    {
    }

    @Override
    public void mouseExited(MouseEvent e)
    {
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {

        if (e.getSource() instanceof FortranEditorPane)
        {
            FortranEditorPane temp = (FortranEditorPane) e.getSource();
            if (temp.getSelectedText() != null && temp.getSelectedText() != "")
            {
                infoSelection.setText(Strings.s("Forms:SelectedChars") + temp.getSelectedText().length());
            }
            else
            {
                infoSelection.setText(Strings.s("Forms:NoSelection"));
            }
        }

    }

    public void newFile()
    {
        Log.send("Adding new file");
        editorPane.addTab("untitled " + (EditorFile.getNewFilesCount() + 1), new EditorFile("", this));
        editorPane.setSelectedIndex(editorPane.getTabCount() - 1);
        ((EditorFile) editorPane.getSelectedComponent()).getEditor().addKeyListener(this);
        ((EditorFile) editorPane.getSelectedComponent()).getEditor().addMouseListener(this);
        updateButtons();
    }

    public void open(String filename)
    {
        if (filename != null && new File(filename).exists() && !isOpenend(filename))
        {
            Log.send("Opening " + "\"" + filename + "\"");
            editorPane.addTab(filename.substring(filename.lastIndexOf("/") + 1, filename.length()),
                    new EditorFile(filename, this));
            editorPane.setSelectedIndex(editorPane.getComponentCount() - 1);
            ((EditorFile) editorPane.getSelectedComponent()).getEditor().addKeyListener(this);
            ((EditorFile) editorPane.getSelectedComponent()).getEditor().addMouseListener(this);
        }
        else if (isOpenend(filename))
        {
            Log.send("File already loaded.");
        }
        else
        {
            // Log.send("Error: Unable to load "+filename);
        }
    }

    public void redo()
    {
        if (editorPane.getSelectedIndex() != -1)
        {
            int i = 0;
            try
            {
                while (((EditorFile) editorPane.getSelectedComponent()).canRedo() && i < 2)
                {
                    ((EditorFile) editorPane.getSelectedComponent()).redo();
                    if (!((EditorFile) editorPane.getSelectedComponent()).getEditor().getDoc()
                            .getText(0,
                                    ((EditorFile) editorPane.getSelectedComponent()).getEditor().getDoc().getLength())
                            .equals(((EditorFile) editorPane.getSelectedComponent()).getEditor().getCourant()))
                    {
                        ((EditorFile) editorPane.getSelectedComponent()).getEditor().updateCourant();
                        i++;
                    }
                }
                if (i >= 2)
                    ((EditorFile) editorPane.getSelectedComponent()).undo();
            } catch (Exception ex)
            {
                ex.printStackTrace();
            }
            updateButtons();
            ((EditorFile) editorPane.getSelectedComponent()).getEditor().updateCourant();
            ((EditorFile) editorPane.getSelectedComponent()).getEditor().addMouseListener(this);

        }
    }

    public boolean saveAll()
    {
        for (Component file : editorPane.getComponents())
        {
            if (file instanceof EditorFile)
            {
                if (!((EditorFile) file).callSave())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        return true;

    }

    public void searchLineInActiveFile(String line, String word)
    {
        ((EditorFile) editorPane.getSelectedComponent()).searchLine(line, word);
    }

    public void undo()
    {
        if (editorPane.getSelectedIndex() != -1)
        {
            try
            {
                while (((EditorFile) editorPane.getSelectedComponent()).canUndo()
                        && ((EditorFile) editorPane.getSelectedComponent()).getEditor().getDoc()
                        .getText(0,
                                ((EditorFile) editorPane.getSelectedComponent()).getEditor().getDoc()
                                        .getLength())
                        .equals(((EditorFile) editorPane.getSelectedComponent()).getEditor().getCourant()))
                {
                    ((EditorFile) editorPane.getSelectedComponent()).undoManager.undo();
                }
            } catch (Exception ex)
            {
                ex.printStackTrace();
            }
            updateButtons();
            ((EditorFile) editorPane.getSelectedComponent()).getEditor().updateCourant();
        }
    }

    public void updateButtons()
    {
        if (editorPane.getSelectedIndex() != -1)
        {

            Set set = toolBarButtons.entrySet();
            Iterator iterator = set.iterator();

            while (iterator.hasNext())
            {
                Map.Entry entry = (Map.Entry) iterator.next();
                JButton button = (JButton) entry.getValue();
                button.setEnabled(true);
            }
            toolBarButtons.get(ButtonAction.UNDO).setEnabled(((EditorFile) editorPane.getSelectedComponent()).canUndo());
            toolBarButtons.get(ButtonAction.REDO).setEnabled(((EditorFile) editorPane.getSelectedComponent()).canRedo());

            if (((EditorFile) editorPane.getSelectedComponent()).isEdited())
            {
                toolBarButtons.get(ButtonAction.SAVE).setEnabled(true);
                toolBarButtons.get(ButtonAction.SAVEAS).setEnabled(true);
            }
            else
            {
                toolBarButtons.get(ButtonAction.SAVE).setEnabled(false);
                toolBarButtons.get(ButtonAction.SAVEAS).setEnabled(false);
            }

        }
        else
        {
            Set set = toolBarButtons.entrySet();
            Iterator iterator = set.iterator();

            while (iterator.hasNext())
            {
                Map.Entry entry = (Map.Entry) iterator.next();
                if (entry.getKey() != ButtonAction.NEW && entry.getKey() != ButtonAction.OPEN)
                {
                    JButton button = (JButton) entry.getValue();
                    button.setEnabled(false);
                }
                else
                {
                    JButton button = (JButton) entry.getValue();
                    button.setEnabled(true);
                }
            }
        }

    }

    public enum ButtonAction
    {
        NEW, OPEN, SAVE, SAVEAS, CLOSE, REDO, UNDO, CUT, COPY, PASTE, SEARCH, REPLACE, BUILD, CLEAR,
        COMPILEF, LINK, RUN
    }

}
