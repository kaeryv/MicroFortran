package io.quadrasoft.mufortran.general;

import io.quadrasoft.mufortran.resources.Strings;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Project
{
    private String name;
    private String path;
    private String filename;
    private String buildPath;
    private String compilerPath;
    private String argument;
    private String executionPath;
    private String executableName;
    private String compilerOption;
    private String Author;
    private Date lastEdit;
    private boolean printLog = false;
    private List<String> source = new ArrayList<>();
    private List<String> externals = new ArrayList<>();

    private boolean selected = false;

    public Project(String arg0)
    {
        if (arg0.contains("\\"))
        {
            setFilename(arg0.replaceAll("\\\\", "/"));
        }
        else
        {
            setFilename(arg0);
        }
        setPath(filename.substring(0, filename.lastIndexOf("/") + 1)); // Path

        if (new File(filename).exists())
        {

            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

            try
            {
                final DocumentBuilder builder = factory.newDocumentBuilder();
                final Document document = builder.parse(new File(filename));

                final Element root = document.getDocumentElement();

                if (root.getTagName() != "project")
                {
                    throw new IOException("Bad project file.");
                }

                this.setName(root.getAttribute("name"));

                final NodeList nodes = root.getChildNodes();
                final int length = nodes.getLength();

                for (int i = 0; i < length; i++)
                {
                    if (nodes.item(i).getNodeType() == Node.ELEMENT_NODE)
                    {
                        final Element node = (Element) nodes.item(i);

                        if (node.getTagName() == "compilation")
                        {
                            compilerPath = node.getAttribute("gfortranCommand");
                            buildPath = node.getAttribute("gfortranBuildPath");
                            compilerOption = node.getAttribute("gfortranFlags");
                            executableName = node.getAttribute("gfortranTarget");

                            String temp = node.getAttribute("gfortranDumpLog");

                            if (temp.equalsIgnoreCase("true"))
                            {
                                printLog = true;
                            }
                            else if (temp.equalsIgnoreCase("false"))
                            {
                                printLog = false;
                            }
                            else
                            {
                                throw new IOException("Bad value for boolean gfortranDumpLog");
                            }
                        }
                        else if (node.getTagName() == "sources")
                        {
                            final NodeList sourceNodes = node.getChildNodes();
                            final int sources_count = sourceNodes.getLength();

                            for (int j = 0; j < sources_count; j++)
                            {
                                if (sourceNodes.item(j).getNodeType() == Node.ELEMENT_NODE)
                                {
                                    final Element sourceFile = (Element) sourceNodes.item(j);
                                    source.add(sourceFile.getAttribute("relativePath"));
                                }
                            }
                        }
                        else if (node.getTagName() == "externals")
                        {
                            final NodeList sourceNodes = node.getChildNodes();
                            final int sources_count = sourceNodes.getLength();

                            for (int j = 0; j < sources_count; j++)
                            {
                                if (sourceNodes.item(j).getNodeType() == Node.ELEMENT_NODE)
                                {
                                    final Element sourceFile = (Element) sourceNodes.item(j);
                                    externals.add(sourceFile.getAttribute("relativePath"));
                                }
                            }
                        }
                        else if (node.getTagName() == "execution")
                        {
                            argument = node.getAttribute("inputArguments");
                            executionPath = node.getAttribute("executionPath");
                        }
                        else if (node.getTagName() == "author")
                        {
                            Author = node.getTextContent();
                        }
                    }
                }

            } catch (ParserConfigurationException e)
            {
                e.printStackTrace();
            } catch (SAXException e)
            {
                e.printStackTrace();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
//LEGACY PROJECT FILE DEPRECATED
/*
            BufferedReader br;
            try {
                br = new BufferedReader(new FileReader(filename));
                String line;
                this.setArgument("");
                this.setBuildPath("bin/");
                this.setAuthor(System.getProperty("user.name"));
                this.setCompilerPath("gfortran");
                this.setCompilerOption("-o");
                this.setExecutableName(getName());
                printLog = false;
                while ((line = br.readLine()) != null) {
                    if (line.contains("\\")) {
                        line = line.replaceAll("\\\\", "/");
                    }
                    if (line.lastIndexOf("::") != -1) {
                        if (line.substring(0, line.lastIndexOf("::")).replaceAll("\\s+", "")
                                .equalsIgnoreCase("source")) {
                            //source.add(line.substring(line.indexOf("\"") + 1, line.lastIndexOf("\"")));
                        } else if (line.substring(0, line.lastIndexOf("::")).replaceAll("\\s+", "")
                                .equalsIgnoreCase("compiler")) {
                            setCompilerPath(line.substring(line.indexOf("\"") + 1, line.lastIndexOf("\"")));
                        } else if (line.substring(0, line.lastIndexOf("::")).replaceAll("\\s+", "")
                                .equalsIgnoreCase("argument")) {
                            setArgument(line.substring(line.indexOf("\"") + 1, line.lastIndexOf("\"")));
                        } else if (line.substring(0, line.lastIndexOf("::")).replaceAll("\\s+", "")
                                .equalsIgnoreCase("buildPath")) {
                            setBuildPath(line.substring(line.indexOf("\"") + 1, line.lastIndexOf("\"")));
                        } else if (line.substring(0, line.lastIndexOf("::")).replaceAll("\\s+", "")
                                .equalsIgnoreCase("exeName")) {
                            setExecutableName(line.substring(line.indexOf("\"") + 1, line.lastIndexOf("\"")));
                        } else if (line.substring(0, line.lastIndexOf("::")).replaceAll("\\s+", "")
                                .equalsIgnoreCase("compOptn")) {
                            this.setCompilerOption(line.substring(line.indexOf("\"") + 1, line.lastIndexOf("\"")));
                        } else if (line.substring(0, line.lastIndexOf("::")).replaceAll("\\s+", "")
                                .equalsIgnoreCase("author")) {
                            this.setAuthor(line.substring(line.indexOf("\"") + 1, line.lastIndexOf("\"")));
                        } else if (line.substring(0, line.lastIndexOf("::")).replaceAll("\\s+", "")
                                .equalsIgnoreCase("executionPath")) {
                            this.setExecutionPath(line.substring(line.indexOf("\"") + 1, line.lastIndexOf("\"")));
                        } else if (line.substring(0, line.lastIndexOf("::")).replaceAll("\\s+", "")
                                .equalsIgnoreCase("external")) {
                            //externals.add(line.substring(line.indexOf("\"") + 1, line.lastIndexOf("\"")));
                        } else if (line.substring(0, line.lastIndexOf("::")).replaceAll("\\s+", "")
                                .equalsIgnoreCase("printlog")) {
                            if (line.substring(line.indexOf("\"") + 1, line.lastIndexOf("\"")).equals("true")) {
                                setPrintLog(true);
                            } else {
                                setPrintLog(false);
                            }
                        }
                    }
                }
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }*/

            if (Session.getActiveProject() != null)
                Session.getActiveProject().setSelected(false);
            this.setSelected(true);
        }
        else
        {
            Log.send("Project file not available.");
        }
    }

    public Project(String project_name, String arg1, String arg2, String compiler_path)
    {
        source.add("main.f90");
        this.setName(project_name);

        // Security for Windows file path
        if (arg1.contains("\\"))
        {
            arg1 = arg1.replaceAll("\\\\", "/") + "/";
        }
        this.setPath(arg1 + name + "/");

        this.setPrintLog(false);
        this.setFilename(path + Strings.s("app:projectfilename") + Strings.s("app:projectextension"));
        this.setBuildPath(arg2);
        this.setArgument("");
        this.setCompilerPath(compiler_path);
        this.setCompilerOption("-o");
        this.setExecutionPath(".");
        this.setAuthor(System.getProperty("user.name"));
        this.setExecutableName(getName());
        if (Session.getActiveProject() != null)
            Session.getActiveProject().setSelected(false);
        this.setSelected(true);
    }

    public Project(String arg0, String arg1, String arg2, String arg3, List<String> arg4)
    {
        setName(arg0);
        if (arg1.contains("\\"))
        {
            arg1 = arg1.replaceAll("\\\\", "/") + "/";
        }
        this.setPath(arg1 + name + "/");
        this.setFilename(path + Strings.s("app:projectfilename") + Strings.s("app:projectextension"));
        this.setBuildPath(arg2);
        this.setCompilerPath(arg3);
        this.setArgument("");
        this.setCompilerOption("-o");
        this.setAuthor(System.getProperty("user.name"));
        this.setExecutableName(getName());
        this.setExecutionPath("./bin/");
        this.setPrintLog(false);
        this.source = arg4;
        if (Session.getActiveProject() != null)
            Session.getActiveProject().setSelected(false);
        this.setSelected(true);
    }

    public void createStartFile() throws IOException
    {
        File file = new File(path + "main.f90");
        if (!file.exists())
        {
            new File(path).mkdirs();
            file.createNewFile();
        }

        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write("program main");
        fileWriter.write(System.getProperty("line.separator"));
        fileWriter.write("implicit none");
        fileWriter.write(System.getProperty("line.separator"));
        fileWriter.write("print \"(A)\",\"Hello World !\"");
        fileWriter.write(System.getProperty("line.separator"));
        fileWriter.write("end program");

        fileWriter.close();
    }

    public String getArgument()
    {
        return argument;
    }

    public void setArgument(String argument)
    {
        this.argument = argument;
    }

    public String getAuthor()
    {
        return Author;
    }

    public void setAuthor(String author)
    {
        Author = author;
    }

    public String getBuildPath()
    {
        return buildPath;
    }

    public void setBuildPath(String buildPath)
    {
        this.buildPath = buildPath;
    }

    public String getCompilerOption()
    {
        return compilerOption;
    }

    public void setCompilerOption(String compilerOption)
    {
        this.compilerOption = compilerOption;
    }

    public String getCompilerPath()
    {
        return compilerPath;
    }

    public void setCompilerPath(String compilerPath)
    {
        this.compilerPath = compilerPath;
    }

    public String getExecutableName()
    {
        return executableName;
    }

    public void setExecutableName(String executableName)
    {
        this.executableName = executableName;
    }

    public String getExecutionPath()
    {
        return executionPath;
    }

    public void setExecutionPath(String executionPath)
    {
        this.executionPath = executionPath;
    }

    public List<String> getExternals()
    {
        return externals;
    }

    public void setExternals(List<String> externals)
    {
        this.externals = externals;
    }

    public String getFilename()
    {
        return filename;
    }

    public void setFilename(String filename)
    {
        this.filename = filename;
    }

    public Date getLastEdit()
    {
        return lastEdit;
    }

    public void setLastEdit(Date lastEdit)
    {
        this.lastEdit = lastEdit;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getObjectPath()
    {
        return "obj/";
    }

    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        this.path = path;
    }

    public List<String> getSource()
    {
        if (source.contains("changelog.txt"))
            source.remove("changelog.txt");
        return source;
    }

    public void setSource(List<String> source)
    {
        this.source = source;
    }

    public boolean isPrintLog()
    {
        return printLog;
    }

    public void setPrintLog(boolean printLog)
    {
        this.printLog = printLog;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    public void save() throws IOException
    {

        // New XML project feature
        File file = new File(filename);
        if (!file.exists())
        {
            new File(path).mkdirs();
            //new File(path + "changelog.txt").createNewFile();
            file.createNewFile();
        }

        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilder builder = null;
        try
        {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e)
        {
            e.printStackTrace();
        }

        final Document document = builder.newDocument();

        final Element root = document.createElement("project");
        root.setAttribute("name", this.getName());
        document.appendChild(root);

        final Element compilationElement = document.createElement("compilation");
        compilationElement.setAttribute("gfortranCommand", compilerPath);
        compilationElement.setAttribute("gfortranBuildPath", buildPath);
        compilationElement.setAttribute("gfortranFlags", compilerOption);
        compilationElement.setAttribute("gfortranTarget", executableName);
        if (printLog)
        {
            compilationElement.setAttribute("gfortranDumpLog", "true");
        }
        else
        {
            compilationElement.setAttribute("gfortranDumpLog", "false");
        }
        root.appendChild(compilationElement);

        final Element executionElement = document.createElement("execution");
        executionElement.setAttribute("inputArguments", argument);
        executionElement.setAttribute("executionPath", executionPath);
        root.appendChild(executionElement);

        final Element sourcesElement = document.createElement("sources");
        root.appendChild(sourcesElement);
        for (String src : this.source)
        {
            final Element fileElement = document.createElement("file");
            fileElement.setAttribute("relativePath", src);
            sourcesElement.appendChild(fileElement);
        }

        final Element externalsElement = document.createElement("externals");
        root.appendChild(externalsElement);
        for (String ext : externals)
        {
            final Element fileElement = document.createElement("file");
            fileElement.setAttribute("relativePath", ext);
            externalsElement.appendChild(fileElement);
        }

        final Element authorElement = document.createElement("author");
        authorElement.setTextContent(Author);
        root.appendChild(authorElement);

        final TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try
        {
            final Transformer transformer = transformerFactory.newTransformer();
            final DOMSource source = new DOMSource(document);

            final StreamResult sortie = new StreamResult(file);


            transformer.setOutputProperty(OutputKeys.VERSION, "1.0");

            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.transform(source, sortie);
        } catch (TransformerConfigurationException e)
        {
            e.printStackTrace();
        } catch (TransformerException e)
        {
            e.printStackTrace();
        }
        //LEGACY

/*
        File file = new File(filename);
        if (!file.exists()) {
            new File(path).mkdirs();
            //new File(path + "changelog.txt").createNewFile();
            file.createNewFile();
        }


        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write("compiler :: \"" + compilerPath + "\"");
        fileWriter.write(System.getProperty("line.separator"));
        fileWriter.write("buildPath :: \"" + buildPath + "\"");
        fileWriter.write(System.getProperty("line.separator"));
        fileWriter.write("argument :: \"" + argument + "\"");
        fileWriter.write(System.getProperty("line.separator"));
        for (String src : this.source) {
            fileWriter.write("source :: \"" + src + "\"");
            fileWriter.write(System.getProperty("line.separator"));
        }
        for (String ext : externals) {
            fileWriter.write("external :: \"" + ext + "\"");
            fileWriter.write(System.getProperty("line.separator"));
        }
        fileWriter.write("author :: \"" + Author + "\"");
        fileWriter.write(System.getProperty("line.separator"));
        fileWriter.write("exeName :: \"" + executableName + "\"");
        fileWriter.write(System.getProperty("line.separator"));
        fileWriter.write("compOptn :: \"" + compilerOption + "\"");
        fileWriter.write(System.getProperty("line.separator"));
        fileWriter.write("executionPath :: \"" + executionPath + "\"");
        fileWriter.write(System.getProperty("line.separator"));
        if (printLog) {
            fileWriter.write("printLog :: \"" + "true" + "\"");
        } else {
            fileWriter.write("printLog :: \"" + "false" + "\"");
        }
        fileWriter.write(System.getProperty("line.separator"));
        fileWriter.close();
        */
    }
}
